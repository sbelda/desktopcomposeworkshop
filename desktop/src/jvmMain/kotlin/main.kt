import com.example.sergiobelda.workshop.common.MultiplatformApp
import androidx.compose.desktop.Window

fun main() = Window {
    MultiplatformApp()
}