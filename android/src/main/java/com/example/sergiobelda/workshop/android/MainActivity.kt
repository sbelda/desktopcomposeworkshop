package com.example.sergiobelda.workshop.android

import com.example.sergiobelda.workshop.common.MultiplatformApp
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MultiplatformApp()
        }
    }
}