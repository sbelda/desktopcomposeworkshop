package com.example.sergiobelda.workshop.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconToggleButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.sergiobelda.workshop.common.model.Song
import com.example.sergiobelda.workshop.common.sampledata.songs
import com.example.sergiobelda.workshop.common.ui.theme.ComposeWorkshopTheme
import com.example.sergiobelda.workshop.common.ui.theme.red600

@Composable
fun MultiplatformApp() {
    ComposeWorkshopTheme {
        LazyColumn {
            items(songs) {
                SongItem(it)
            }
        }
    }
}

@Composable
fun SongItem(song: Song) {
    var saved by remember { mutableStateOf(song.saved) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp)
            .clickable(onClick = { /* Ignore */ }),
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconToggleButton(
            checked = saved,
            onCheckedChange = { saved = it },
            modifier = Modifier.padding(start = 8.dp, end = 8.dp)
        ) {
            if (saved) {
                Icon(
                    Icons.Default.Favorite,
                    contentDescription = "Saved",
                    tint = red600
                )
            } else {
                Icon(
                    Icons.Default.FavoriteBorder,
                    contentDescription = "Not saved",
                    tint = MaterialTheme.colors.onSurface
                )
            }
        }
        Text(
            text = song.number.toString(),
            modifier = Modifier.padding(end = 8.dp),
            color = MaterialTheme.colors.onSurface
        )
        Text(
            text = song.title,
            modifier = Modifier.weight(1f),
            color = MaterialTheme.colors.onSurface
        )
        Text(
            text = "3:45",
            modifier = Modifier.padding(end = 16.dp),
            color = MaterialTheme.colors.onSurface
        )
    }
}