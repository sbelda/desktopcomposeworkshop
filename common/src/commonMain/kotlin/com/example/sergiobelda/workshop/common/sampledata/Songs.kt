package com.example.sergiobelda.workshop.common.sampledata

import com.example.sergiobelda.workshop.common.model.Song

val song1 =
    Song(
        id = 1,
        number = 1,
        title = "The Adults Are Talking",
        artistId = 1,
        saved = true
    )

val song2 =
    Song(
        id = 2,
        number = 2,
        title = "Selfless",
        artistId = 1,
        saved = true
    )

val song3 =
    Song(
        id = 3,
        number = 3,
        title = "Brooklyn Bridge To Chorus",
        artistId = 1,
        saved = true
    )

val song4 =
    Song(
        id = 4,
        number = 4,
        title = "Bad Decisions",
        artistId = 1,
        saved = true
    )

val song5 =
    Song(
        id = 5,
        number = 5,
        title = "Eternal Summer",
        artistId = 1,
        saved = true
    )

val song6 =
    Song(
        id = 6,
        number = 6,
        title = "At The Door",
        artistId = 1,
        saved = true
    )

val song7 =
    Song(
        id = 7,
        number = 7,
        title = "Why Are Sundays So Depressing",
        artistId = 1,
        saved = false
    )

val songs = listOf(song1, song2, song3, song4, song5, song6, song7)